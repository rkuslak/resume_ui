import React from "react";
import { ISkill } from "interfaces";

export function JobSkillsList(props: { skills?: Array<ISkill>; companyName: string; }) {
  const { skills, companyName } = props;

  if (!skills || skills.length < 1) return <></>;

  const skillCategories = new Map();
  skills.forEach(skill => {
    var category = skillCategories.get(skill.skillType) || []
    category.push(skill);
    skillCategories.set(skill.skillType, category);
  })

  return <ul>
    {
      Array.from(skillCategories.keys()).map(skillCategory => {
        const categorySkills: Array<ISkill> = skillCategories.get(skillCategory);

        return <>
          <li key={`${companyName}-${skillCategory}-header`} className="jobSkill category">
            {skillCategory}:
          </li>
          <ul key={`${companyName}-${skillCategory}`}>
            {
              categorySkills.map((skill: ISkill) => (
                <JobSkillListItem key={skill.name + "-" + companyName} skill={skill} />)
              )
            }
          </ul>
        </>;
      })
    }
  </ul>
}

function JobSkillListItem(props: { skill: ISkill }) {
  const { skill } = props;
  if (!skill) return <></>
  return <li className="jobSkill">{skill.name}</li>
}
