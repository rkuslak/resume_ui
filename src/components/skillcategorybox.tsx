import React from "react";
import { ISkill } from "interfaces";

import "./skillcategorybox.scss";
import { DateTime } from "luxon";

export function SkillCategoryBox(props: { categoryName: string; skills: ISkill[]; }) {
  const { categoryName, skills } = props;

  return (
    <div>
      <span className="skillListCategoryName">{categoryName}</span>
      <div className="skillListSkillBox">
        {
          skills.map(skill => {
            return <SkillsListCard key={`${categoryName}-${skill.name}`} skill={skill} />;
          })
        }
      </div>
    </div>
  );
}

function yearsFromDateTime(datetime: DateTime): string {
  var difference = datetime.diffNow(["years", "milliseconds"]);
  return (difference.years * -1).toString();
}




function SkillsListCard(props: { skill: ISkill; }) {
  const { skill } = props;

  return (
    <div className="skillListSkillItem">
      <div className="skillListSkillItem spacer"></div>
      <div className="skillListSkillItem itemName">{skill.name}</div>
      { skill.years && <div className="skillListSkillItem byLine">Years: {typeof(skill.years) === "number" ? skill.years : yearsFromDateTime(skill.years) } </div>}
      <div className="skillListSkillItem spacer"></div>
    </div>
  );
}
