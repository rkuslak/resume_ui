import React from "react";

import "./jobaccomplishmentlist.scss";

export function JobAccomplishmentsList(props: { accomplishments?: string | string[]; companyName: string; }) {
  if (!props.accomplishments) {
    return <> </>;
  }

  const { companyName } = props;
  const accomplishments = typeof (props.accomplishments) === "string" ?
    [props.accomplishments] :
    props.accomplishments;

  return (<ul className="achievement">
    {accomplishments.map((line, idx) => (
      <li key={companyName + "-" + idx} className="achievement">
        {line}
      </li>))}
  </ul>
  );
}
