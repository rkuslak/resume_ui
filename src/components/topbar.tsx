import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { NavLink as Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedin, faGithubSquare, faGitlab } from '@fortawesome/free-brands-svg-icons';

import "./topbar.scss";

const links = [
  { icon: faLinkedin, link: "https://linkedin.com/in/ronkuslak" },
  { icon: faGithubSquare, link: "https://github.com/rkuslak" },
  { icon: faGitlab, link: "https://gitlab.com/rkuslak" },
];

const activities = [
  { name: "Home", destination: "/" },
  { name: "Experience", destination: "/experience" },
  { name: "Projects", destination: "/projects" },
  { name: "Resume", destination: "/resume" },
  // { name: "Contact", destination: "/contact" },
]

export function TopBar() {
  return (
    <Navbar
      bg="dark"
      variant="dark"
      sticky="top"
      style={{ marginBottom: "1em" }}
      expand="md"
    >
      <Navbar.Brand href="/" className="topBarLogoText">RonKuslak.com</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {
            activities.map((activity, idx) => <Link
              activeClassName="active"
              className="topBarLinks nav-link"
              key={"navlink-" + idx}
              exact={activity.destination === "/"}
              to={activity.destination}
              
            >
              {activity.name}
            </Link>
            )
          }
        </Nav>
        <Nav className="h3">
          {
            links.map((link, idx) => {
              return <Nav.Link href={link.link} key={"links-" + idx}>
                <FontAwesomeIcon icon={link.icon}></FontAwesomeIcon>
              </Nav.Link>
            })
          }
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}