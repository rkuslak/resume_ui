import React from "react";
import { DATE_FORMAT } from "global_constants";
import { IJobCardProps } from "interfaces";

export default function JobCard(props: IJobCardProps) {
  const { job, currentJob, setJob } = props;
  function onClickHandler() { setJob(job); }

  const classes = "jobCard " + ((currentJob === job) ? "jobCardActive" : "");

  return <div className={classes} onClick={() => onClickHandler()}>
    {job.logo && <img alt={job.companyName + " logo"} className="cardLogo" src={job.logo} />}
    <div className="jobCardBody">
      <span className="company">{job.companyName}</span><br />
      <span className="title">{job.title}</span><br />
      <span className="daterange">
        {job.startDate.toFormat(DATE_FORMAT)}
        {job.endDate && " - " + job.endDate.toFormat(DATE_FORMAT)}
      </span>
    </div>
  </div>;
}
