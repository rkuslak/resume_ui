import React from "react";

import "./jobnotes.scss";

export function JobNotes(props: { notes?: string | string[]; companyName: string; }) {
  if (!props.notes) {
    return <> </>;
  }
  const { companyName } = props;
  const notes = (typeof (props.notes) === "string") ? [props.notes] : props.notes;

  return <>
    {notes.map((note, idx) => <p className="jobNote" key={`${companyName}-${idx}`}>
      {note}
    </p>
    )}
  </>;
}
