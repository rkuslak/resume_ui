import React, { useState, useEffect } from "react";
import { ISkill } from "interfaces";
import { getSkillCategories } from "requests/resume";
import { SkillCategoryBox } from "components/skillcategorybox";
export function SkillsBox() {
  const [skillCategories, setSkillCategories] = useState<Map<string, Array<ISkill>> | null>(null);

  useEffect(() => {
    var skillCategories = getSkillCategories();
    setSkillCategories(skillCategories);
  }, []);

  return <div key="SkillsArray">
    {skillCategories &&
      Array.from(skillCategories.keys()).map(categoryName => {
        return <SkillCategoryBox
          key={`sb-${categoryName}`}
          categoryName={categoryName}
          skills={skillCategories.get(categoryName) || []} />;
      })}
  </div>;
}
