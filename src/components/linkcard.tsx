import React from 'react';
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Badge from "react-bootstrap/Badge";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLink } from "@fortawesome/free-solid-svg-icons";
import { faGithub } from "@fortawesome/free-brands-svg-icons";

import "./linkcard.scss";

export interface cardProps {
  title: string,
  technologies?: string[] | undefined | null,
  description: string,
  linkAddress?: string | undefined | null,
  githubRepo?: string | undefined | null,
}

interface ITagArrayProps {
  title: string
  techs: string[] | null | undefined
}

function TagArray(props: ITagArrayProps) {
  var {title, techs} = props;

  if(!techs) {
    return <></>;
  }

  return <>{
    techs.map(tech => {
        var key = title + "-" + tech;
        return <Badge key={key} className="techBadge" variant="dark">
          {tech}
        </Badge>
    })
  }</>;
}

export default function LinkCard(props: cardProps) {
  var { technologies, title, description, linkAddress, githubRepo } = props;

  return <>
    <Card className="text-left h-100">
      <Card.Header className="text-center">{title}</Card.Header>
      <Card.Body>
        <Card.Text>{description}</Card.Text>
        <TagArray title={title} techs={technologies} />
        <div className="buttonBox">
          {
            linkAddress && <>
              <Button variant="primary" href={linkAddress} className="cardButton" size="sm">
                <FontAwesomeIcon icon={faLink} /> Link
              </Button>
            </>
          }
          {
            githubRepo && <>
              <Button
                variant="secondary"
                href={"https://github.com/" + githubRepo}
                className="cardButton"
                size="sm"
              >
                <FontAwesomeIcon icon={faGithub} /> Repository
              </Button>
            </>
          }
        </div>
      </Card.Body>
    </Card>
  </>;
}