import { IJob, ISkill } from "interfaces";
import { DateTime } from "luxon";

export function getJobs(): Array<IJob> {
  var jobs: Array<IJob> = [];
  const skills = getSkills();
  // TODO: This goes away with the API. Just saying.
  const mappedSkills = new Map();

  skills.forEach(skill => mappedSkills.set(skill.name, skill));
  function getMappedSkills(skillList: Array<string>): Array<ISkill> {
    return skillList.map(skillName => {
      var s = mappedSkills.get(skillName)
      if (!s) {console.log(skillName)}
      return s;
    })
  }


  jobs = [
    {
      companyName: "C. H. Robinson",
      logo: require("images/C.H.-Robinson-1.jpg"),
      title: "Support Developer II",
      skills: getMappedSkills(["MSSQL", "MongoDB", "Docker", "C#", "Javascript", "Typescript", "React", "Angular"]),
      description: [
        `Developed and debugged our internal applications using Angular and React and APIs written in C#`,
        `Provided assistance and support for our state of the art live systems via data manipulation on `
        + `SQL Server and MongoDB`,
        `Provided support for our live applications and APIs with data stored `
        + `on MongoDB, MSSQL and Oracle databases`,
      ],
      startDate: DateTime.fromISO("2018-08-01"),
      endDate: DateTime.fromISO("2019-09-15"),
    },
    {
      companyName: "ScienceLogic",
      logo: require("images/sciencelogic.png"),
      title: "Software Engineer",
      skills: getMappedSkills(["MySQL", "Python", "Django", "Prometheus", "Kafka", "DGraph", "Redis", "Scylla", "Cassandra", "Kubernetes"]),
      description: [
        "Designed and implemented self health metrics orchestartion for our Agent pipeline backend services",
        "Refactored and further developed the back end pipeline for our agent based monitoring pipeline",
      ],
      startDate: DateTime.fromISO("2019-09-15"),
    },
    {
      companyName: "Bankoe Companies",
      logo: require("images/Ban-Koe.png"),
      title: "Implementation Specialist",
      skills: getMappedSkills(["SQL Server", "PowerShell", "C#"]),
      description: [
        `Developed and implemented custom software and planned solutions `
        + `for customers using PASCAL, TSQL, C#, batch and PowerShell script. `
        + `Personally designed and developed back end application to automate stand up and `
        + `bridging of data between our applications using C# and Dapper.`,
        `Remotely installed software using various remote desktop technologies, `
        + `including RDP, WebEx, and TeamViewer.`,
        `Remotely assisted with the stand up and administion of Windows and MS SQL Server instances.`,
        `Managed communication with client and provided in depth training on software.`,
        `Provided technical guidance to team members for SQL, PowerShell, and `
        + `general Windows administration and prepared and presented teaching on PowerShell coding.`,
      ],
      startDate: DateTime.fromISO("2016-12-01"),
      endDate: DateTime.fromISO("2018-08-01"),
    },

  ]

  jobs = jobs.sort((y: IJob, x: IJob) => {
    // As we're sorting dates, dates with a larger year are first, then a
    // larger month, then a larger day. If any comparison returns 0, we know
    // they are the same and will evaluate as falsey, moving to the next
    // comparison
    const x_d = x.startDate;
    const y_d = y.startDate;
    return x_d.year - y_d.year ||
      x_d.month - y_d.month ||
      x_d.day - y_d.day ||
      0
  })

  return jobs;
}

export function getSkills(): Array<ISkill> {
  var skills: Array<ISkill> = [
    {
      skillType: "Languages",
      name: "C#",
      description: [
        "Multiple years of experience writing C# applications on the back end. ",
        "Versed and capable of working with and writing AspNet and AspNetCore on the backend.",
      ],
    },
    {
      name: "C++",
      skillType: "Languages",
    },
    {
      name: "Rust",
      years: DateTime.fromISO("2017-01-01"),
      skillType: "Languages",
    },
    {
      name: "Go",
      years: DateTime.fromISO("2017-01-01"),
      skillType: "Languages",
    },
    {
      name: "Python",
      years: DateTime.fromISO("2004-01-01"),
      skillType: "Languages",
    },
    {
      name: "Javascript",
      skillType: "Languages",
    },
    {
      name: "Typescript",
      skillType: "Languages",
    },
    {
      name: "Java",
      description: "Working with Java since 2006. Versed in Spring.",
      years: DateTime.fromISO("2006-01-01"),
      skillType: "Languages",
    },
    {
      name: "React",
      skillType: "Technologies",
    },
    {
      name: "Angular",
      skillType: "Technologies",
    },
    {
      name: "MSSQL",
      skillType: "Technologies",
    },
    {
      name: "TSQL",
      skillType: "Technologies",
    },
    {
      name: "MongoDB",
      skillType: "Technologies",
    },
    {
      name: "VueJS",
      skillType: "Technologies",
    },
    {
      name: "Docker",
      skillType: "Technologies",
    },
    {
      name: "Bootstrap",
      skillType: "Technologies",
    },
    {
      name: "Linux",
      skillType: "Technologies",
    },
    {
      name: "Visual Studio Code",
      skillType: "Tools",
    },
    {
      name: "Visual Studio",
      skillType: "Tools",
    },
    {
      name: "Git (Gitlab / Github)",
      skillType: "Tools",
    },
    {
      name: "Maven",
      skillType: "Tools",
    },
    {
      name: "MySQL",
      skillType:"Technologies",
    },
    {
      name: "DGraph",
      skillType: "Technologies",
    },
    {
      name: "PowerShell",
      skillType: "Technologies",
    },
    {
      name: "Kafka",
      skillType: "Technologies",
    },
    {
      name: "Django",
      skillType: "Technologies",
    },
    {
      name: "Cassandra",
      skillType: "Technologies",
    },
    {
      name: "Prometheus",
      skillType: "Technologies",
    },
    {
      name: "SQL Server",
      skillType: "Technologies",
    },
    {
      name: "npm",
      skillType: "Tools",
    },
    {
      name: "webpack",
      skillType: "Tools",
    },
    {
      name: "Kubernetes",
      skillType: "Technologies",
    },
    {
      name: "Redis",
      skillType: "Technologies",
    },
    {
      name: "Scylla",
      skillType: "Technologies",
    },
  ]

  return skills;
}

export function getSkillCategories(): Map<string, Array<ISkill>> {
  var categories: Map<string, Array<ISkill>> = new Map();
  var skills = getSkills();

  skills.forEach(skill => {
    const { skillType } = skill;
    if (!categories.has(skillType)) {
      categories.set(skillType, []);
    }
    const skillArray = categories.get(skillType) || [];
    skillArray.push(skill);
    categories.set(skillType, skillArray);
  });

  return categories;
}