import React from 'react';
import { TopBar } from "components/topbar";
import Intro from "routes/intro";
import Resume from "routes/resume";
import Contact from "routes/contact";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Container from "react-bootstrap/Container";
import { CSSTransition, TransitionGroup } from "react-transition-group";

import './App.scss';
import Projects from './routes/projects';
import { Experience } from 'routes/experience';

const App: React.FC = () => {
  return (
    <div className="App">
      <Router>
        <TopBar />
        <Container fluid={true}>
          <Route render={({ location }) => (
            <TransitionGroup>
              <CSSTransition key={location.key} classNames="fade" timeout={700}>
                <Switch location={location}>
                  <Route exact path="/" component={Intro} />
                  <Route path="/experience" component={Experience} />
                  <Route path="/projects" component={Projects} />
                  <Route path="/resume" component={Resume} />
                  <Route path="/contact" component={Contact} />
                </Switch>
              </CSSTransition>
            </TransitionGroup>
          )} />
        </Container>
      </Router>
    </div>
  );
}

export default App;
