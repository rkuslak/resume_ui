import { DateTime } from "luxon";

export interface IJob {
  companyName: string;
  title: string;
  description: string | Array<string>;
  body?: string | Array<string>;
  startDate: DateTime;
  endDate?: DateTime;
  skills?: Array<ISkill>;
  logo?: string;
}

export type SkillType = "Languages" | "Technologies" | "Tools";

export interface ISkill {
  name: string;
  skillType: SkillType;
  description?: string | string[];
  years?: number | DateTime;
}

export interface ISkillCategory {
  name: string;
  skills: Array<ISkill>;
}

type SetJobCallback = (arg0: IJob | null) => void;

export interface IJobCardProps {
  job: IJob
  currentJob: IJob | null
  setJob: SetJobCallback
}


