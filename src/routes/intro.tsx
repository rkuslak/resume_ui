import React, { useLayoutEffect, useState } from "react";

import "./intro.scss";

export default function Intro() {
  const [sizeConstrained, setSizeConstrained] = useState<string>("");

  useLayoutEffect(() => {

    function updateSizeStyles() {
      // console.log(window.innerWidth);
      setSizeConstrained(window.innerWidth < 600 ? "" : "introMargins");
    }
    updateSizeStyles();

    window.addEventListener('resize', updateSizeStyles);
    return () => window.removeEventListener('resize', updateSizeStyles);
  }, []);

  return <div>
    <div className={`introHeader ${sizeConstrained}`}>
      <h1>Ron Kuslak</h1>
      <p className="">Software Engineer</p>
      <p>Creative Technology for a Better Tomorrow</p>
    </div>

    <div className={`introBody ${sizeConstrained}`}>
      <p>
        I am an skilled and experienced software engineer with several years
        experience in designing and delivering solutions using my knowledge in
        many varied languages, environments and disciplines. I have deep knowledge
        of C#, C++, Rust, Python, Javascript/Typescript, React,  and Vue. I have
        experience with MongoDB, OracleDB, Postgre SQL, and extensive experience
        with MSSQL Server.
    </p>
      <p>
        On this site I've collected information on my skills and previous
        accomplishments, provided in a more standard "resume" form and in more
        interactive views as well. Additionally, some links to and additional
        information on projects I've worked on can be found here as well. I look
        forward to working with you soon.
    </p>
    </div>
  </div>
}