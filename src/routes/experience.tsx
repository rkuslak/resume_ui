import React, { useState, useEffect } from "react";

import { IJob } from "interfaces";
import { getJobs } from "requests/resume";

import "./experience.scss"
import { JobSkillsList } from "components/jobskillslist";
import { JobNotes } from "components/jobnotes";
import JobCard from "components/jobcard";
import { JobAccomplishmentsList } from "components/jobaccomplishmentlist";

export function Experience() {
  const [currentJob, setCurrentJob] = useState<IJob | null>(null);
  const [jobs, setJobs] = useState<Array<IJob>>([]);

  useEffect(() => {
    const newJobs = getJobs();
    setJobs(newJobs);
  }, [])

  return <>
    <div className="jobBox">
      {
        jobs.map((job, idx) => (
          <JobCard
            key={`jobcard-${idx}`}
            job={job}
            currentJob={currentJob}
            setJob={setCurrentJob}
          />))
      }
    </div>
    <div className="jobBodyBox">
      {
        currentJob && <>
          <JobSkillsList companyName={currentJob.companyName} skills={currentJob.skills} />
          <JobNotes companyName={currentJob.companyName} notes={currentJob.body} />
          <JobAccomplishmentsList companyName={currentJob.companyName} accomplishments={currentJob.description} />
        </>
      }
    </div>
  </>
}