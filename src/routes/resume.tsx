import React, { useState, useEffect } from "react";
import { IJob } from "interfaces";

import { getJobs } from "requests/resume";

import { DATE_FORMAT } from "global_constants";

import "./resume.scss"
import { JobSkillsList } from "components/jobskillslist";
import { JobAccomplishmentsList } from "components/jobaccomplishmentlist";
import { SkillsBox } from "../components/skillsbox";


function ExperienceBox() {
  const [jobs, setExperiences] = useState<Array<IJob>>([])


  useEffect(() => {
    var jobs = getJobs();

    setExperiences(jobs);
  }, []);

  return <>
    <ul className="jobs">
      {
        jobs.map((job, idx) => {
          return <div className="positionHeader" key={job.companyName + "-" + idx}>
            <span className="company">{job.companyName}</span><br />
            <span className="title">{job.title}</span><br />
            <span className="daterange">
              {job.startDate.toFormat(DATE_FORMAT)}
              {job.endDate && " - " + job.endDate.toFormat(DATE_FORMAT)}
            </span>
            <JobSkillsList key={`jobskilllist-${job.companyName}-${idx}`} skills={job.skills} companyName={job.companyName} />
            <JobAccomplishmentsList key={`jobaccomp-${idx}`} accomplishments={job.description} companyName={job.companyName} />

            {job.logo && <img alt={job.companyName + " logo"} className="logo" src={job.logo} />}

          </div>
        })
      }

    </ul>
  </>
}

export default function Resume() {
  return (<div>
    <div className="stickyHeader text-light"><p>Skills</p></div>
    <SkillsBox />
    <div className="stickyHeader text-light"><p>Experience</p></div>
    <ExperienceBox />
  </div>)
}