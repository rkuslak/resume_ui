import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import LinkCard, { cardProps } from "components/linkcard";

const siteProjects: cardProps[] = [
  {
    title: "GroupCalc",
    technologies: ["React", "Websockets"],
    description: "An simple example of some of my work with various web technologies. Written using Golang, React and websockets.",
    linkAddress: "http://ronkuslak.com/groupcalc/index.html",
    githubRepo: "rkuslak/groupcalc.ui",
  },
  {
    title: "GroupCalc - Websocket API",
    technologies: ["Golang", "Websockets"],
    description: "Provides a restful endpoint and WebSocket interface for the GroupCalc web app.",
    githubRepo: "rkuslak/groupcalc.service",
  },
  {
    title: "MultiBuilder",
    technologies: ["Rust", "JSON", "Yaml"],
    description: "Command line application to orchestrate various build systems. Attempts to provide a single application as means to kick off many build processes",
    githubRepo: "rkuslak/multibuilder",
  },
];

export default function Projects() {
  return <div>
    <Row>
      <Col className={"text-center"}>
        <h3>Selected Projects</h3>
      </Col>
    </Row>
    <Row>
      {
        siteProjects.map((link, idx) => <Col sm={12} md={6} lg={4} style={{ marginTop: 6 }} key={"linkcard-" + idx}>
          <LinkCard {...link} />
        </Col>)
      }
    </Row>
  </div>

}